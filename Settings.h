#ifndef MS_HACKMAN_SETTINGS
#define MS_HACKMAN_SETTINGS

#include <vector>
#include <string>

struct Settings{
	public:
	int Timebank;
	int TimePerMove;
	std::vector<std::string> PlayerNames;
	std::string MyName;
	int MyID;
	int Width;
	int Height;
	int MaxRounds;
};


#endif