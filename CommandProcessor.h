#ifndef MS_HACKMAN_COMMAND_PROCESSOR
#define MS_HACKMAN_COMMAND_PROCESSOR

#include <string>

class Bot;

class CommandProcessor{
	public:
	std::string GetNextCommand();
	std::string Process(Bot* bot);

	std::string ProcessSettings(Bot* bot);
	std::string ProcessUpdate(Bot* bot);
	std::string ProcessAction(Bot* bot);
};

#endif