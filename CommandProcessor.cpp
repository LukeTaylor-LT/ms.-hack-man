#include "CommandProcessor.h"
#include "Bot.h"
#include "Util.h"

#include <iostream>


#include <ctime>

std::string CommandProcessor::GetNextCommand() {
	std::string data;
	std::cin >> data;
	return data;
}

std::string CommandProcessor::Process(Bot* bot){
	std::string command = this->GetNextCommand();
	std::string output = "";

	//int start = clock();

	if (command == "settings") {
		output = this->ProcessSettings(bot);

	}
	else if (command == "update") {
		output = this->ProcessUpdate(bot);

	}
	else if (command == "action") {
		output = this->ProcessAction(bot);

	}

	/*int end = clock();

	std::cout << end - start << std::endl;*/

	return output;
}


std::string CommandProcessor::ProcessSettings(Bot* bot) {
	std::string type = this->GetNextCommand();
	std::string setting = this->GetNextCommand();

	if (type == "timebank") {
		bot->settings.Timebank = Utils::SToI(setting.c_str());
		//return "set timebank to " + setting;
	}
	else if (type == "time_per_move") {
		bot->settings.TimePerMove = Utils::SToI(setting.c_str());
		//return "set time per move to " + setting;
	}
	else if (type == "player_names") {
		bot->settings.PlayerNames.clear();
		size_t found = setting.find(",");
		bot->settings.PlayerNames.push_back(setting.substr(0, found));
		setting.erase(0, found + 1);
		bot->settings.PlayerNames.push_back(setting);
		//return "set player names";

	}
	else if (type == "your_bot") {
		bot->settings.MyName = setting;
		//return "set bot name to " + setting;

	}
	else if (type == "your_botid") {
		bot->settings.MyID = Utils::SToI(setting.c_str());
		//return "set bot id to " + setting;

	}
	else if (type == "field_width") {
		bot->settings.Width = Utils::SToI(setting.c_str());
		bot->field.SetH(bot->settings.Width);
		//return "set width to " + setting;

	}
	else if (type == "field_height") {
		bot->settings.Height = Utils::SToI(setting.c_str());
		bot->field.SetW(bot->settings.Height);
		//return "set height to " + setting;

	}
	else if (type == "max_rounds") {
		bot->settings.MaxRounds = Utils::SToI(setting.c_str());
		//return "set rounds to " + setting;
	}

	return "";
}

std::string CommandProcessor::ProcessUpdate(Bot* bot) {
	std::string target = this->GetNextCommand();
	std::string type = this->GetNextCommand();
	std::string data = this->GetNextCommand();

	if (type == "round") {
		bot->round = Utils::SToI(data.c_str());

	}
	else if (type == "field") {
		bot->field.Update(data,bot->settings.MyID + '0');
		bot->x = bot->field.MyX;
		bot->y = bot->field.MyY;

		//bot->field.Output(bot);

	} //no other updates are that important at the moment
	
	return "";
}

std::string CommandProcessor::ProcessAction(Bot* bot) {
	std::string action = this->GetNextCommand();
	std::string time = this->GetNextCommand();

	if (action == "character") {
		return "bixiette";

	}
	else if (action == "move") {
		return bot->Move();
	}
}