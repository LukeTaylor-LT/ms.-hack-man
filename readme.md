# Ms. Hack-man

Ms. Hack-man takes **Boris** deep into the Booking.com server room. Collect code snippets faster than your opponent, while avoiding bugs. Use laser-mines to your advantage, but be careful of the blast!

*This competition is run by Booking.com on riddles.io. At the Season One Lockdown, __Boris__ (v40) finished 53/67 with 571 point, just 5 points away from the bot in 52nd place.*