#include "Field.h"

#include "Util.h"


void Field::Initialise(int w, int h) {
	this->SetW(w);
	this->SetH(h);
}

void Field::SetW(int w) {
	this->Width = w;
	this->Data = std::vector<std::vector<CellType>>(w);
	this->SetH(this->Height);
}

void Field::SetH(int h) {
	this->Height = h;
	for (int x = 0; x < this->Width; x++) {
		this->Data[x].resize(h);
	}
}

Field::Field(){}

void Field::Update(std::string data, char ID) {
	std::string cell;
	std::string cellItem;
	size_t found;
	size_t f;
	int x = 0;
	int y = 0;

	while (true) {
		found = data.find(",");
		if (found == std::string::npos) { //last cell
			cell = data;
		}
		else {
			cell = data.substr(0, found);
			data.erase(0, found + 1);
		}

		while (true) {
			f = cell.find(";");
			if (f == std::string::npos) { //last item in cell
				cellItem = cell;
			}
			cellItem = cell.substr(0, found);
			cell.erase(0, found + 1);

			if (cellItem[0] == 'P') {
				if (cellItem[1] == ID) {
					this->Data[x][y] = Player;
					this->MyX = x;
					this->MyY = y;
					break;
				}
				/*else {
					std::cout << cellItem << " - " << ID << std::endl;
				}*/
			}

			/***** stuff we wanna know regardless of what else is on the cell:
			/* Walls (so nothing else on that cell)
			/* Gates (ditto)
			/* Bugs (so stay away!)
			/* Active Mines (ditto)
			/*****/

			if (cellItem[0] == 'x') {
				this->Data[x][y] = Blocked;
				break;

			}
			else if (cellItem[0] == 'G') {
				if (cellItem[1] == 'l') {
					this->Data[x][y] = GateLeft;
				}
				else {
					this->Data[x][y] = GateRight;
				}
				break;

			}
			else if (cellItem[0] == 'E') {
				this->Data[x][y] = Bug;
				break;

			}
			else if (cellItem[0] == 'B') {
				if (cellItem != "B") { //Unarmed mines are just "B" whereas armed mines have the amount of turns until they blow
					this->Data[x][y] = ArmedMine;
					break;
				}
				else {
					this->Data[x][y] = Mine;
					//no break - this isn't an urgent threat
				}

			}

			/***** lower priority:
			/* Snippet (would rather know if there's a bug/mine!)
			/* Bug Spawn (not an immediate danger unless we are nearing a bug spawn turn)
			/* Empty Cell (well, it's empty)
			/*****/

			else if (cellItem[0] == 'C') {
				this->Data[x][y] = Snippet;

			}
			else if (cellItem[0] == 'S') {
				this->Data[x][y] = BugSpawn;

			}
			else {
				this->Data[x][y] = Empty;
			}

			if (f == std::string::npos) {
				break;
			}
		}

		if (found == std::string::npos) {
			break;
		}

		y++;
		if (y >= this->Height) {
			y = 0;
			x++;
		}

	}
}

bool Field::IsWall(int x, int y){
	CellType c = this->Data[x][y];
	return (c == Blocked);
}

//std::string Field::Output(Bot* bot) {
//	std::string response = "";
//	for (int x = 0; x < bot->field.Width; x++) {
//		for (int y = 0; y < bot->field.Height; y++) {
//			response += " ";
//			switch (bot->field.Data[x][y])
//			{
//			case Blocked:
//				response += "X";
//				break;
//
//			case Snippet:
//				response += "C";
//				break;
//
//			case GateLeft:
//				response += "L";
//				break;
//
//			case GateRight:
//				response += "R";
//				break;
//
//			case Player:
//				response += "P";
//				break;
//
//			case Target:
//				response += "T";
//				break;
//
//			default:
//				response += "-";
//			}
//			response += " ";
//		}
//		response += "\n";
//	}
//	return response;
//}