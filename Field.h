#ifndef MS_HACKMAN_FIELD
#define MS_HACKMAN_FIELD

#include <vector>
#include <string>

class Bot;

enum CellType {
	Empty,
	Blocked,
	Player,
	Opponent,
	BugSpawn,
	GateLeft,
	GateRight,
	Bug,
	Mine,
	ArmedMine,
	Snippet,
	Target
};

class Field{
	public:
	Field();
	void Initialise(int w, int h);
	void SetW(int w);
	void SetH(int h);

	void Update(std::string data,char ID);
	bool IsWall(int x, int y);

	//std::string Output(Bot* b);
	
	int MyX;
	int MyY;

	int Width;
	int Height;
	std::vector<std::vector<CellType>> Data;
};

#endif