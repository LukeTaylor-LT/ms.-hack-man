#ifndef MS_HACKMAN_POINT
#define MS_HACKMAN_POINT

class Point{
public:
	int x;
	int y;
	Point() {}
	Point(int x, int y);
	bool const operator<(const Point &a);
	bool const operator==(const Point &a);
};


#endif