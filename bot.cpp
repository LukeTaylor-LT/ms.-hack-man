#include "Bot.h"

#include <random>

Bot::Bot(){
	
}

Point Bot::getNearestSnippet() {
	int range = 1;
	while (true) {
		for (int x = this->x - range; x < this->x + range; x++) {
			if (x >= this->settings.Height || x < 0) {
				continue;
			}
			for (int y = this->y - range; y < this->y + range; y++) {
				if (y >= this->settings.Width || y < 0) {
					continue;
				}
				if (this->field.Data[x][y] == Snippet) {
					return Point(x,y);
				}
				/*else {
					std::cout << x << "," << y << " is " << field.Data[x][y] << std::endl;
				}*/
			}
		}
		range++;
	}
}

std::string Bot::Move() {
	if (this->field.Data[this->target.x][this->target.y] != Snippet) {
		this->target = this->getNearestSnippet();
	}
	//this->field.Data[target.x][target.y] = Target;

	bool triedMoves[4] = { false,false,false,false };
	std::string moves[4] = {"left","right","up","down" };
	std::string prevMoves[4] = { "right","left","down","up" };

	std::string move;
	

	int targetMove;

	if (this->y < target.y && !triedMoves[1]) { //right
		targetMove = 1;
	}

	if (this->y > target.y && !triedMoves[0]) { //left
		targetMove = 0;
	}

	if (this->x < target.x && !triedMoves[3]) { //down
		targetMove = 3;
	}

	if (this->x > target.x && !triedMoves[2]) { //up
		targetMove = 2;
	}

	bool canMove = this->checkMove(moves[targetMove]);
	if (canMove && this->lastMove != prevMoves[targetMove]) {
		move = moves[targetMove];
		this->lastMove = move;
		return move;
	}
	else {
		triedMoves[targetMove] = true;
	}

	for (int i = 0; i < 4; i++) {
		targetMove = -1;

		if (this->y < target.y && !triedMoves[1]) { //right
			targetMove = 1;
		}

		if (this->y > target.y && !triedMoves[0]) { //left
			targetMove = 0;
		}

		if (this->x < target.x && !triedMoves[3]) { //down
				targetMove = 3;
		}

		if (this->x > target.x && !triedMoves[2]) { //up
				targetMove = 2;
		}

		if (this->lastMove == prevMoves[targetMove] || triedMoves[targetMove] == true || targetMove == -1) {
			continue;
		}

		if (this->checkMove(moves[targetMove])) {
			move = moves[targetMove];
			this->lastMove = move;
			return move;
		}
		else {
			triedMoves[targetMove] = true;
		}
	}

	srand(0);
	move = moves[rand() % 4];

	if (this->checkMove(move)) {
		move = moves[targetMove];
		this->lastMove = move;
		return move;
	}

	move = "pass";
	this->lastMove = move;
	return move; // +"\n" + this->field.Output(this);
}

bool Bot::checkMove(std::string move) {
	Point intendedPoint = this->getMoveCoordinates(move);
	bool canMove = !this->field.IsWall(intendedPoint.x, intendedPoint.y);
	return canMove;
}

Point Bot::getMoveCoordinates(std::string move) {
	int targetX = 0;
	int targetY = 0;
	if (move == "right") {
		targetX = this->x;
		targetY = this->y + 1;
	}
	else if (move == "left") {
		targetX = this->x;
		targetY = this->y - 1;
	}
	else if (move == "up") {
		targetX = this->x - 1;
		targetY = this->y;
	}
	else if (move == "down") {
		targetX = this->x + 1;
		targetY = this->y;

	}
	return Point(targetX, targetY);
}