#include "Point.h"

Point::Point(int x,int y) : x(x), y(y){}

bool const Point::operator<(const Point &a){
	return x < a.x || (x == a.y && y < a.y);
}

bool const Point::operator==(const Point &a){
	return x == a.x && y == a.y;
}