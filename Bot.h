#ifndef MS_HACKMAN_BOT
#define MS_HACKMAN_BOT

#include "Settings.h"
#include "Field.h"
#include "Point.h"

class Bot{
public:
	Bot();

	Settings settings;
	Field field;
	int round;
	int x;
	int y;
	std::string lastMove;
	Point target;

	Point getNearestSnippet();
	std::string Move();
	bool checkMove(std::string move);
	Point getMoveCoordinates(std::string move);
	
};


#endif