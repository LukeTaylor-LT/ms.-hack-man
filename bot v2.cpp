#include "Bot.h"
#include "CommandProcessor.h"

#include <iostream>

int main(){
	Bot* b = new Bot();;
	CommandProcessor processor;
	std::string output;

	while (true) {
		output = processor.Process(b);
		if (output != "") {
			std::cout << output << std::endl;
		}
	}
}